﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetCameraPivot : MonoBehaviour
{
    [SerializeField]
    private Transform _parentTransform;

    [SerializeField]
    private GameObject _resetPivotObject;

    public void ResetPivot()
    {
        this._parentTransform.position = _resetPivotObject.transform.position;
    }
}
