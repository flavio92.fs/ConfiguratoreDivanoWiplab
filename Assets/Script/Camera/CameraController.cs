﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CameraController : MonoBehaviour
{
    private Transform _cameraTransform;
    private Transform _parentTransform;

    private Vector3 _localRotation;

    private float _cameraDistance;

    [SerializeField] private float _mouseSensitivity;
    [SerializeField] private float _orbitDampening;
    [SerializeField] private bool orbitX;

    private Vector3 _defaultCameraPosition;
    private Quaternion _defaultCameraParentRotation;


    //void Start()
    //{
    //    _cameraDistance = _cameraTransform.localPosition.z * -1;
    //}

    public void InitializeCameraController()
    {
        _cameraTransform = this.transform;
        _parentTransform = this.transform.parent;

        _defaultCameraPosition = this.transform.localPosition;
        _defaultCameraParentRotation = this.transform.localRotation;

    }

    public void CameraOrbit()
    {
        Quaternion qt;
        _localRotation.y += Input.GetAxis("Mouse Y") * -_mouseSensitivity;
        _localRotation.y = Mathf.Clamp(_localRotation.y, 0, 90);

        if (orbitX)
        {
            _localRotation.x += Input.GetAxis("Mouse X") * _mouseSensitivity;
            qt = Quaternion.Euler(_localRotation.y, _localRotation.x, 0);
        }
        else
        {
            qt = Quaternion.Euler(_localRotation.y, 0, 0);
        }
        this._parentTransform.rotation = Quaternion.Lerp(transform.rotation, qt, Time.deltaTime * _orbitDampening);
    }

    public IEnumerator ResetView(Action completedCallback)
    {
        float waitTime = 0.5f;
        float elapsedTime = 0;
        Quaternion currentRotation = _parentTransform.rotation;
        Vector3 currentPosition = _cameraTransform.localPosition;
        
        while (elapsedTime < waitTime)
        {
            _parentTransform.rotation = Quaternion.Lerp(currentRotation, _defaultCameraParentRotation, (elapsedTime / waitTime));
            _cameraTransform.localPosition = Vector3.Lerp(currentPosition, _defaultCameraPosition, (elapsedTime / waitTime));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        _localRotation = Vector3.zero;

        completedCallback();
    }
}
