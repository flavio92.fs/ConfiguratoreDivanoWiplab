﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomCollision : MonoBehaviour
{
    private float defaultDistance;

    [SerializeField]
    private float minDistance = 1f;

    [SerializeField]
    private float maxDistance = 4f;
    [SerializeField]
    private float smooth = 10f;
    [SerializeField]
    private Vector3 dollyDir;
    [SerializeField]
    private Vector3 dollyDirAdjusted;

    [SerializeField]
    private float distance;

    [SerializeField]
    private float scrollSensitivity;

    



    // Start is called before the first frame update
    void Awake()
    {
        dollyDir = transform.localPosition.normalized;
        distance = transform.localPosition.magnitude;

        defaultDistance = distance;

        EventSystem.OnResetZoom += ResetDistance;
    }

    private void OnDestroy()
    {
        EventSystem.OnResetZoom -= ResetDistance;
    }

    // Update is called once per frame
    void Update()
    {
        ZoomCollisions();
        
    }

    private void ZoomCollisions() 
    {
        Quaternion qt = this.transform.rotation;
        Vector3 fwd = qt * Vector3.forward;

        Vector3 desiredCameraPosition = transform.TransformPoint(dollyDir * maxDistance);
        RaycastHit hit;

        Debug.DrawRay(transform.position, fwd.normalized * 5, Color.red);

        if (Physics.Raycast(transform.position, fwd, out hit))
        {
            if (hit.distance < minDistance)
            {
                distance += 1;
            }

            if (hit.distance > minDistance)
            {
                if (Input.GetAxis("Mouse ScrollWheel") != 0)
                {
                    float scrollAmount = Input.GetAxis("Mouse ScrollWheel") * -scrollSensitivity;

                    distance += scrollAmount;
                }
            }
        }

        transform.localPosition = Vector3.Lerp(transform.localPosition, dollyDir * distance, Time.deltaTime * smooth);
    }

    private void ResetDistance()
    {
        distance = defaultDistance;
    }




}
