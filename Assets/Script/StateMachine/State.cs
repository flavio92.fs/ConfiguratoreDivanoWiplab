﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class State
{
    public AppSystem _appSystem;

    public State(AppSystem appSystem)
    {
        _appSystem = appSystem;
    }

    public virtual void DoState()
    {

    }

    public virtual void Initialize()
    {

    }

    public virtual void Idle()
    {

    }

    public virtual void ResetView()
    {

    }

    public virtual void CameraOrbit()
    {

    }
}
