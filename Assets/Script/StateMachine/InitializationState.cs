﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializationState : State
{
    public InitializationState(AppSystem _appSystem) : base(_appSystem)
    {

    }
    public override void DoState()
    {
        Initialize();
    }

    public override void Initialize()
    {
        _appSystem.boundsSystem.GetAllChildren();
        _appSystem.boundsSystem.GetCenterPivotAndVolume();
        _appSystem.resetCameraPivot.ResetPivot();
        _appSystem.cameraController.InitializeCameraController();
        _appSystem.SetState(_appSystem.idleState);
    }
}
