﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State
{
    public IdleState(AppSystem _appSystem) : base(_appSystem)
    {

    }

    public override void DoState()
    {
        if (Input.GetMouseButton(0))
        {
            _appSystem.SetState(_appSystem.orbitState);
        }
    }
}
