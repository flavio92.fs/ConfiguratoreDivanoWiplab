﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppSystem : MonoBehaviour
{
    public State _currentState;

    public State inizializationState;
    public State idleState;
    public State orbitState;
    public State resetState;

    public CameraController cameraController;
    public ResetCameraPivot resetCameraPivot;
    public BoundsSystem boundsSystem;
    

    private void Awake()
    {
        inizializationState = new InitializationState(this);
        idleState = new IdleState(this);
        orbitState = new OrbitState(this);
        resetState = new ResetState(this);
    }

    private void Start()
    {
        SetState(inizializationState);
        Debug.Log(_currentState.ToString());
    }

    private void Update()
    {
        _currentState.DoState();
        Debug.Log(_currentState.ToString());
    }

    public void SetState(State state)
    {
        _currentState = state;
    }

    public void OnAppStart()
    {
        _currentState.Initialize();
    }

    public void OnIdle()
    {
        _currentState.Idle();
    }

    public void OnMouseClickAndDrag()
    {
        _currentState.CameraOrbit();
    }

    public void OnResetButton()
    {
        SetState(new ResetState(this));
        _currentState.ResetView();
    }
}
