﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitState : State
{
    public OrbitState(AppSystem _appSystem) : base(_appSystem)
    {

    }

    private Vector3 _localRotation;

    public override void DoState()
    {
        if (Input.GetMouseButton(0))
        {
            CameraOrbit();
        }
        
        else
        {
            _appSystem.SetState(_appSystem.idleState);
        }
    }

    public override void CameraOrbit()
    {
        _appSystem.cameraController.CameraOrbit();
    }
}
