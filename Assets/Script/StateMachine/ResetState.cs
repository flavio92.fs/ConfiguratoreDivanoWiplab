﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetState : State
{

    public ResetState (AppSystem _appSystem)  : base(_appSystem)
    {

    }

    public override void ResetView()
    {
        //Callback da studiare!!! (Delegates Action)
        _appSystem.StartCoroutine(_appSystem.cameraController.ResetView(() => {
            _appSystem.SetState(_appSystem.idleState);
        }));
    }
}
