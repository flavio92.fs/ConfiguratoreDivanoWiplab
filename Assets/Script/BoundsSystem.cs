﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsSystem : MonoBehaviour
{
    [SerializeField]
    private GameObject cover;
    
    [SerializeField]
    private GameObject[] furnitureParts;

    public void GetAllChildren() 
    {
        int children = transform.childCount;
        
        furnitureParts = new GameObject[children];

        for (int i = 0; i < children; i++)
        {
            furnitureParts[i] = transform.GetChild(i).gameObject;
        }
    }


    //Trovo il centro e il volume dei children
    public void GetCenterPivotAndVolume()
    {
        Renderer rend = new Renderer();
        Bounds b = new Bounds();
        foreach(GameObject furniturePart in furnitureParts)
        {
            if (furniturePart.activeInHierarchy)
            {
                rend = furniturePart.GetComponent<Renderer>();
                b.Encapsulate(rend.bounds);
            }
        }

        cover.transform.position = b.center;
        cover.transform.localScale = b.size;

    }










}
