﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomController : MonoBehaviour
{
    private Transform _cameraTransform;
    private float _cameraDistance;

    [SerializeField]
    private float scrollSensitivity;

    [SerializeField]
    private float cameraDistanceMaxValue;
    [SerializeField]
    private float cameraDistanceMinValue;

    Vector3 cameraDirection;

    RaycastHit hit;

    [SerializeField]
    GameObject Sphere;

    [SerializeField]
    GameObject distance;


    // Start is called before the first frame update
    void Start()
    {
        _cameraTransform = this.transform;
        _cameraDistance = _cameraTransform.localPosition.z * -1;

        cameraDirection = _cameraTransform.localPosition.normalized;
    }

    private void FixedUpdate()
    {
        //CameraRaycastDistance();
    }

    private void LateUpdate()
    {
        CameraZoom();
    }

    private void CameraZoom()
    {

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            float scrollAmount = Input.GetAxis("Mouse ScrollWheel") * -scrollSensitivity;

            this._cameraDistance += scrollAmount * 1f;

            this._cameraDistance = Mathf.Clamp(this._cameraDistance, cameraDistanceMinValue, cameraDistanceMaxValue);

            if (_cameraTransform.localPosition.z != this._cameraDistance)
            {
                this._cameraTransform.localPosition = new Vector3(0, 0, this._cameraDistance * -1f);
            }
        }
    }

    private void CameraRaycastDistance()
    {
        Quaternion qt = this.transform.rotation;
        Vector3 fwd = qt * Vector3.forward;
        Vector3 left = qt * Vector3.left;
        Vector3 right = qt * Vector3.right;
        Vector3 middleLeft = qt * (Vector3.forward - Vector3.right);
        Vector3 middleRight = qt * (Vector3.forward + Vector3.right);

        RaycastHit hit;
        Ray cameraRay = new Ray(this.transform.position, fwd);
        Ray leftRay = new Ray(this.transform.position, left);
        Ray rightRay = new Ray(this.transform.position, right);

        Ray middleLeftRay = new Ray(this.transform.position, middleLeft);
        Ray middleRightRay = new Ray(this.transform.position, middleRight);

        if (Physics.Raycast(cameraRay, out hit, 5f))
        {
            this.transform.localPosition = new Vector3(0, 0, this.transform.localPosition.z - 0.1f);
        }

        if (Physics.Raycast(leftRay, out hit, 5f))
        {
            this.transform.localPosition = new Vector3(0, 0, this.transform.localPosition.z - 2f);
        }

        if (Physics.Raycast(rightRay, out hit, 5f))
        {
            this.transform.localPosition = new Vector3(0, 0, this.transform.localPosition.z - 2);
        }

        if (Physics.Raycast(middleLeftRay, out hit, 5f))
        {
            this.transform.localPosition = new Vector3(0, 0, this.transform.localPosition.z - 1f);
        }

        if (Physics.Raycast(middleRightRay, out hit, 5f))
        {
            this.transform.localPosition = new Vector3(0, 0, this.transform.localPosition.z - 1f);
        }
    }

    private void DebugDrawRay()
    {
        Quaternion qt;

        qt = this.transform.rotation;
        Vector3 fwd = qt * (distance.transform.localPosition - Vector3.forward);
        Vector3 left = qt * Vector3.left;
        Vector3 right = qt * Vector3.right;

        Vector3 middleLeft = qt * (Vector3.forward - Vector3.right);
        Vector3 middleRight = qt * (Vector3.forward + Vector3.right);

        Ray cameraRay = new Ray(this.transform.position, fwd);
        Ray leftRay = new Ray(this.transform.position, Vector3.left);


        if (Physics.Raycast(leftRay, out hit)) 
        {
            Sphere.transform.position = hit.point;
        }

        Debug.DrawRay(this.transform.position, fwd, Color.red);
        Debug.DrawRay(this.transform.position, left * 10, Color.red);
        Debug.DrawRay(this.transform.position, right * 10, Color.red);
        Debug.DrawRay(this.transform.position, middleLeft * 7.5f, Color.red);
        Debug.DrawRay(this.transform.position, middleRight * 7.5f, Color.red);
    }
}
