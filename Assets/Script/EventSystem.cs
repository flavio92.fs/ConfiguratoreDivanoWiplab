﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventSystem : MonoBehaviour
{
    public static Action ResetCameraPivot;
    public static Action OnResetView;
    public static Action OnResetZoom;
}
