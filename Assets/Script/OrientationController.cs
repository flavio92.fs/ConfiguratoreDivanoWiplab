﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationController : MonoBehaviour
{
    [SerializeField]
    private float rotationSensitivity = 10f;
    private Quaternion defaultObjectRotation;
    private Vector3 defaultObjectPosition;

    [SerializeField]
    private Transform centerPivot;


    // Start is called before the first frame update
    private void Awake()
    {
        EventSystem.OnResetView += StartResetObjetOrientation;
    }

    private void OnDestroy()
    {
        EventSystem.OnResetView -= StartResetObjetOrientation;
    }

    void Start()
    {
        defaultObjectRotation = this.transform.rotation;
        defaultObjectPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float rotationX = Input.GetAxis("Mouse X") * -rotationSensitivity;
        float rotationY = Input.GetAxis("Mouse Y") * -rotationSensitivity;

        if (Input.GetMouseButton(0))
        {
            //transform.Rotate(0,rotationX, 0, Space.World);
            transform.RotateAround(centerPivot.transform.position, Vector3.up, rotationX);
        }
    }

    private IEnumerator ResetObjectOrientation()
    {
        float waitTime = 0.5f;
        float elapsedTime = 0;
        Quaternion currentRotation = this.transform.rotation;
        Vector3 currentPosition = this.transform.position;

        Debug.Log(currentRotation.eulerAngles.y);

        while(elapsedTime < waitTime)
        {
            this.transform.rotation = Quaternion.Lerp(currentRotation, defaultObjectRotation, (elapsedTime/waitTime));
            this.transform.position = Vector3.Lerp(currentPosition, defaultObjectPosition,(elapsedTime / waitTime));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        this.transform.rotation = defaultObjectRotation;
        this.transform.position = defaultObjectPosition;

    }

    private void StartResetObjetOrientation()
    {
        StartCoroutine(ResetObjectOrientation());
    }
}
