﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ObjectSelection : MonoBehaviour, IBeginDragHandler
{
    [SerializeField]
    private LayerMask layerMask;

    private Vector3 mousePos;
    private Vector3 currentMousePos;

    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        CheckMouseClickOrClickDrag();
        SelectObject();
    }

    private void SelectObject()
    {
        Ray ray2 = Camera.main.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray2.origin, ray2.direction * 100f, Color.red);

        if (mousePos == currentMousePos && Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * Mathf.Infinity, Color.red);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                hit.collider.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
            }
        }
    }

    private void CheckMouseClickOrClickDrag()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mousePos = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            currentMousePos = Input.mousePosition;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {

        throw new System.NotImplementedException();
    }
}
